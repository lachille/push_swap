# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lachille <lachille@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/26 17:11:30 by santa             #+#    #+#              #
#    Updated: 2020/03/12 06:56:33 by lachille         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap
NAME2 = checker

CC = gcc
CFLAGS = -Wall -Wextra -Werror -g3
AR = ar rc
RM = rm -rf
	
LIBFT = libft
LIBFT_A = $(LIBFT)/libft.a
INCDIR = includes
OBJDIR = objects
SRCDIR = sources

SOURCES = 	main_solver.c \
			ope.c \
			parsing.c \
			push.c \
			rotate.c \
			solver.c \
			sort.c \
			swap.c \
			utils.c \
			print.c \

SOURCES2 =	main_checker.c \
			checker.c \
			ope.c \
			parsing.c \
			push.c \
			rotate.c \
			swap.c \
			sort.c \
			solver.c \
			utils.c \
			print.c \

INC = push_swap.h

SRCS = $(addprefix $(SRCDIR)/, $(SOURCES))
OBJS = $(addprefix $(OBJDIR)/, $(SOURCES:.c=.o))
OBJS2 = $(addprefix $(OBJDIR)/, $(SOURCES2:.c=.o))

all : $(LIBFT_A) $(OBJDIR) $(NAME) $(NAME2)


$(NAME) : $(INCDIR)/$(INC)  $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LIBFT)/libft.a -o $(NAME)

$(NAME2) : $(INCDIR)/$(INC) $(OBJS2)
	$(CC) $(CFLAGS) $(OBJS2) $(LIBFT)/libft.a -o $(NAME2)

$(LIBFT_A) :
	make -j -C $(LIBFT)

$(OBJDIR) :
	mkdir -p $(OBJDIR)

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -I $(INCDIR) -c $< -o $@ 

clean :
	make -C $(LIBFT) clean
	$(RM) $(OBJS)
	$(RM) $(OBJS2)

fclean : clean
	make -C $(LIBFT) fclean
	$(RM) $(NAME)
	$(RM) $(NAME2)

re :
	make fclean
	make all

debug: $(INCDIR)/$(INC) $(OBJS) $(OBJS2)
	make -C $(LIBFT)
	$(CC) $(CFLAGS) -g3 $(OBJS) $(LIBFT)/libft.a -o $(NAME)
	$(CC) $(CFLAGS) -g3 $(OBJS2) $(LIBFT)/libft.a -o $(NAME2)

debugS: $(INCDIR)/$(INC) $(OBJS) $(OBJS2)
	make debug -C $(LIBFT)
	$(CC) $(CFLAGS) -fsanitize=address $(OBJS) $(LIBFT)/libft.a -o $(NAME)
	$(CC) $(CFLAGS) -fsanitize=address $(OBJS2) $(LIBFT)/libft.a -o $(NAME2)
