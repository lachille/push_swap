
# Push swap

a 42 project where we should sort a list of number with 2 stacks and a set of specific commands, of course the algorithm should use the fewer command possible.

the commands are :

	- SA : Swap A : swap the first 2 elements at the top of stack A
	- SB : swap B : swap the first 2 elements at the top of stack B
	- SS : SA and SB at the same time
	
	- PA : Push A : take the first element at the top of stack B to the top of stack A
	- PB : Push B : take the first element at the top of stack A to the top of stack B

	- RA : Rotate A : shift up all elements of stack A by 1, the first element become the last one.
	- RB : Rotate B : shift up all elements of stack B by 1, the first element become the last one.
	- RR : RA and RB at the same time.

	- RRA : Reverse rotate A : shift down all elements of stack A by 1, the last element become the first one.
	- RRB : Reverse rotate B : shift down all elements of stack B by 1, the last element become the first one.
	- RRR : RRA and RRB at the same time.

For more information see the [subject](subject)

## Deployment

To deploy this project run

```bash
  make
```

You should set argument like:

```bash
  ARG="10 5 2 8 42"
```
then 
```bash
./push_swap $ARG | ./checker $ARG
```
Push_swap is the program who solve the problem and give the commands like:
```bash
ra
ra
pb
ra
pb
rr
ra
pb
rr
pb
```
then checker will use those commands to see if it works.

checker has options:
 
	-p to print both stack for each step
	-f to use the command stocked in a file

if you put the result of push_swap in a file like :
```bash
./push_swap $ARG > result
```
you could access it with
```bash
./checker -f result $ARG
```
with -p option you can use :
```bash
./checker -f result -p $ARG
or
./checker -p -f result $ARG
```
iF you want to generate big series of numbers you can use `ruby`
```bash
ARG=`ruby -e "puts (0..50).to_a.shuffle.join(' ')"`
```