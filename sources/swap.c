/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 01:39:05 by lachille          #+#    #+#             */
/*   Updated: 2020/01/27 02:34:06 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ope_sa(t_tab *tab)
{
	int tmp;

	tmp = tab->a[0];
	tab->a[0] = tab->a[1];
	tab->a[1] = tmp;
	tab->ope.sa = 1;
}

void	ope_sb(t_tab *tab)
{
	int tmp;

	tmp = tab->b[0];
	tab->b[0] = tab->b[1];
	tab->b[1] = tmp;
	tab->ope.sb = 1;
}
