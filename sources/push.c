/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 01:38:32 by lachille          #+#    #+#             */
/*   Updated: 2020/03/03 05:33:45 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

/*
** Push 1st number of pile b on pile a
*/

void	ope_pa(t_tab *tab)
{
	int i;
	int *tmp;

	if (tab->index_b >= 0)
	{
		tab->index_a++;
		if (!(tmp = malloc(sizeof(int) * (tab->index_a + 1))))
			exit(1);
		i = 0;
		tmp[0] = tab->b[0];
		while (++i <= tab->index_a)
			tmp[i] = tab->a[i - 1];
		i = -1;
		while (++i < tab->index_b)
			tab->b[i] = tab->b[i + 1];
		tab->index_b -= tab->index_b >= 0 ? 1 : 0;
		i = -1;
		while (++i <= tab->index_a)
			tab->a[i] = tmp[i];
		free(tmp);
		tab->ope.pa = 1;
	}
}

/*
** Push 1st number of pile a on pile b
*/

void	ope_pb(t_tab *tab)
{
	int	i;
	int *tmp;

	if (tab->index_a >= 0)
	{
		tab->index_b++;
		if (!(tmp = malloc(sizeof(int) * (tab->index_b + 1))))
			exit(1);
		i = 0;
		tmp[0] = tab->a[0];
		while (++i <= tab->index_b)
			tmp[i] = tab->b[i - 1];
		i = -1;
		while (++i < tab->index_a)
			tab->a[i] = tab->a[i + 1];
		tab->index_a -= tab->index_a >= 0 ? 1 : 0;
		i = -1;
		while (++i <= tab->index_b)
			tab->b[i] = tmp[i];
		free(tmp);
		tab->ope.pb = 1;
	}
}
