/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 07:18:29 by lachille          #+#    #+#             */
/*   Updated: 2020/02/19 09:42:09 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	merge2(int *arr, int *temp, t_sort *sort)
{
	if (arr[sort->i] <= arr[sort->j])
	{
		temp[sort->k] = arr[sort->i];
		sort->i += 1;
	}
	else
	{
		temp[sort->k] = arr[sort->j];
		sort->j += 1;
	}
	sort->k += 1;
}

static void	merge(int *arr, int start, int mid, int end)
{
	int		temp[end - start + 1];
	t_sort	sort;

	sort.i = start;
	sort.j = mid + 1;
	sort.k = 0;
	while (sort.i <= mid && sort.j <= end)
		merge2(arr, temp, &sort);
	while (sort.i <= mid)
	{
		temp[sort.k] = arr[sort.i];
		sort.k += 1;
		sort.i += 1;
	}
	while (sort.j <= end)
	{
		temp[sort.k] = arr[sort.j];
		sort.k += 1;
		sort.j += 1;
	}
	sort.i = start - 1;
	while (++sort.i <= end)
		arr[sort.i] = temp[sort.i - start];
}

void		merge_sort(int *arr, int start, int end)
{
	int mid;

	if (start < end)
	{
		mid = (start + end) / 2;
		merge_sort(arr, start, mid);
		merge_sort(arr, mid + 1, end);
		merge(arr, start, mid, end);
	}
}
