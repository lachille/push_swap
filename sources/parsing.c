/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 06:26:49 by lachille          #+#    #+#             */
/*   Updated: 2020/03/12 03:54:58 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int		check(char **av, int mode)
{
	int i;
	int j;

	i = mode;
	if (!av[mode + 1])
	{
		ft_putstr("Error : ARG not define\n");
		return (0);
	}
	while (av[++i])
	{
		j = -1;
		while (av[i][++j])
		{
			if (!ft_isdigit(av[i][j]) && !is_nrvt(av[i][j]) && av[i][j] != '-'
				&& av[i][j] != '+')
				return (0);
			if ((av[i][j] == '-' || av[i][j] == '+') && (!av[i][j + 1]
				|| !ft_isdigit(av[i][j + 1])))
				return (0);
		}
	}
	return (1);
}

/*
** Realloc un tableau de long
*/

int		*realloc_int(int *old, int *buf)
{
	int	*new;
	int	buf2;
	int	i;

	buf2 = *buf;
	*buf *= 2;
	i = -1;
	if (*buf < buf2 || !(new = malloc(sizeof(int) * *buf)))
		exit(1);
	while (++i < *buf)
		if (i < buf2)
			new[i] = old[i];
	free(old);
	return (new);
}

char	*search_number(char *str, int *j)
{
	char	*dst;
	int		i;
	int		tmp;

	i = -1;
	if (!str[*j])
		return (NULL);
	while (is_nrvt(str[*j]) && str[*j])
		*j += 1;
	tmp = *j;
	while (!is_nrvt(str[*j]) && str[*j])
		*j += 1;
	if (!(dst = ft_strnew(*j - tmp + 1)))
		return (0);
	while (tmp < *j)
	{
		dst[++i] = str[tmp];
		tmp += 1;
	}
	return (dst);
}

int		fill_tab2(t_tab *tab, int *i, char **src, int *buf)
{
	int		j;
	long	tmp;
	char	*str;

	j = 0;
	while ((str = search_number(src[*i], &j)))
	{
		if (!*str)
			break ;
		if (tab->index_a >= *buf - 1)
			tab->a = realloc_int(tab->a, buf);
		if ((tmp = ft_atol(str)) > 2147483647 || tmp < -2147483648)
		{
			ft_putstr("One ARG is larger than a int\n");
			free(tab->a);
			free(str);
			return (1);
		}
		tab->a[tab->index_a] = (int)tmp;
		tab->index_a += 1;
		free(str);
	}
	free(str);
	return (0);
}

void	fill_ref(t_tab *tab)
{
		int i;

		i = -1;
		if (!(tab->ref = malloc(sizeof(int) * (tab->index_a + 1))))
			exit (1);
		tab->index_ref = tab->index_a;
		while (++i <= tab->index_ref)
			tab->ref[i] = tab->a[i];
		merge_sort(tab->ref, 0, tab->index_ref);
		tab->med = (tab->index_ref + 1) / 2;
}

/*
** Rempli pile a (int) avec les argument (char*)
** Check si chaques nombre est un int
*/

int		fill_tab(char **src, t_tab *tab, int mode)
{
	int		i;
	int		buf;
	int		res;

	buf = 2;
	i = mode;
	if (!(tab->a = malloc(sizeof(int) * (buf))))
		return (1);
	tab->index_a = 0;
	while (src[++i])
	{
		if (fill_tab2(tab, &i, src, &buf))
			return (1);
	}
	tab->index_a -= 1;
	fill_ref(tab);
	if ((res = doublons_checker(tab)))
	{
		ft_putstr("Double detected : ");
		ft_putnbr(res);
		ft_putstr("\n");
		free(tab->a);
		return (1);
	}
	return (0);
}
