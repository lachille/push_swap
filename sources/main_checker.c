/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_checker.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/03 01:54:40 by lachille          #+#    #+#             */
/*   Updated: 2020/03/12 06:33:09 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	check_valid_command(t_check *chk)
{
	if (!chk->ope)
	{
		free(chk->dst);
		ft_putstr("Usage: ./checker [opt] [ARG]\n-p to \
print step by step\n-f [file] to read a file\n");
		exit(1);
	}
}

void		checker2(t_tab *tab, t_check *chk, int fd)
{
	int i;

	while ((i = read(fd, chk->buf, chk->size_buf)) > 0)
	{
		chk->buf[i] = '\0';
		chk->dst = ft_strjoin_free(chk->dst, chk->buf, 1);
	}
	if (i == -1)
	{
		ft_putstr("Read error\n");
		exit (1);
	}
	free(chk->buf);
	chk->opt & 1 ? print_output(*chk, tab) : 0;
	if (!(tab->b = malloc(sizeof(int) * (tab->index_a + 1))))
		exit(1);
	chk->i = 0;
	while ((chk->ope = ope_finder(chk->dst, &chk->i)) >= 0)
	{
		check_valid_command(chk);
		check_ope(tab, *chk);
	}
	print_res(*tab, *chk);
	free(chk->dst);
	free(tab->a);
	free(tab->b);
	free(tab->ref);
}

int			main(int ac, char **av)
{
	t_tab	tab;
	t_check	chk;
	int		fd;

	fd = 0;
	chk.opt = 0;
	if (ac < 2)
	{
		ft_putstr_fd("Usage: ./checker [opt] [ARG]\n-p to \
print step by step\n-f [file] to read a file\n", 2);
		return (1);
	}
	init_checker(&chk, &tab);
	opt_handler(&chk, av, &fd);
	if (!(chk.buf = ft_strnew(chk.size_buf))
	|| !(chk.dst = ft_strnew(0))
		|| !check(av, chk.opt) || fill_tab(av, &tab, chk.opt))
	{
		free(chk.buf);
		free(chk.dst);
		ft_putstr_fd("Usage: ./checker [opt] [ARG]\n-p to \
print step by step\n-f [file] to read a file\n", 2);
		return (1);
	}
	checker2(&tab, &chk, fd);
	return (0);
}
