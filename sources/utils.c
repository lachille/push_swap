/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/25 13:06:20 by lachille          #+#    #+#             */
/*   Updated: 2020/03/06 07:54:55 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	print_tab(t_tab *tab)
{
	int	i;

	i = -1;
	ft_putstr("\e[32mA : ");
	while (++i <= tab->index_a)
	{
		ft_putstr("|");
		ft_putnbr(tab->a[i]);
		ft_putstr("|");
	}
	ft_putstr("\n");
	i = -1;
	ft_putstr("\e[36mB : ");
	while (++i <= tab->index_b)
	{
		ft_putstr("|");
		ft_putnbr(tab->b[i]);
		ft_putstr("|");
	}
	ft_putstr("\n");
	i = -1;
	ft_putstr("\e[33mMED : ");
	ft_putnbr(tab->med);
	ft_putstr("\n\e[0m");
}

int		doublons_checker(t_tab *tab)
{
	int		res;
	int		i;
	res = 0;
	i = -1;
	while (++i < tab->index_ref)
		if (tab->ref[i] == tab->ref[i + 1])
			res = tab->ref[i];
	return (res);
}

int		is_nrvt(char c)
{
	if (c == ' ' || c == '\n' || c == '\r' || c == '\v' || c == '\t')
		return (1);
	return (0);
}

void		opt_handler(t_check *chk, char **av, int *fd)
{
	if (!ft_strcmp(av[1], "-f"))
	{
		if ((*fd = open(av[2], O_RDONLY)) < 0)
		{
			ft_putstr_fd("Can't open the file\n", 2);
			exit(1);
		}
		chk->opt += 2;
	}
	if ((!chk->opt && !ft_strcmp(av[1], "-p"))
		|| (chk->opt && av[3] && !ft_strcmp(av[3], "-p")))
		chk->opt += 1;
	if (chk->opt == 1 && av[2] && !ft_strcmp(av[2], "-f"))
	{
		if ((*fd = open(av[3], O_RDONLY)) < 0)
		{
			ft_putstr_fd("Can't open the file\n", 2);
			exit(1);
		}
		chk->opt += 2;
	}
}
