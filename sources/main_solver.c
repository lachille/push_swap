/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_solver.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/12 22:31:51 by lachille          #+#    #+#             */
/*   Updated: 2020/03/12 06:02:03 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	main(int ac, char **av)
{
	// int		i;
	t_tab	tab;

	tab.index_b = -1;
	// i = -1;
	(void)ac;
	if (!check(av, 0) || fill_tab(av, &tab, 0))
	{
		ft_putstr("Usage: ./push_swap [ARG]\n");
		return (0);
	}
	solver(&tab);
	free(tab.a);
	free(tab.b);
	free(tab.ref);
	return (0);
}
