/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ope.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 01:19:59 by lachille          #+#    #+#             */
/*   Updated: 2020/03/12 07:13:48 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	reset_ope(t_ope *ope)
{
	ope->sa = 0;
	ope->sb = 0;
	ope->ra = 0;
	ope->rb = 0;
	ope->rra = 0;
	ope->rrb = 0;
	ope->pa = 0;
	ope->pb = 0;
}

void	try_use_double_ope(t_tab *tab)
{
	//faire une mediane dynamique de b (se refresh a chaques turn pour choisir l'action )
	if (tab->index_b > 0 && tab->b_status == NOT_SORT)
	{
		if (tab->ope.ra && (tab->b[0] < tab->b[tab->index_b]))
			ope_rb(tab);
		else if (tab->ope.rra && (tab->b[tab->index_b] > tab->b[tab->index_b - 1]))// || (tab->b[0] < tab->b[tab->index_b]
				 //&& tab->b[0] < tab->b[tab->index_b - 1])))
			ope_rrb(tab);
		else if (tab->ope.sa && tab->b[0] < tab->b[1])
			ope_sb(tab);
		// else if (!tab->ope.pb && tab->b[0] < tab->a[0])
		// 	ope_pa(tab);
	}
}

void	find_next_inf_med(t_tab *tab)
{
	int i;
	int j;

	i = -1;
	j = -1;
	while(tab->a[++i] && tab->a[i] >= tab->med);
	while(tab->a[tab->index_a - ++j] && tab->a[tab->index_a - j] >= tab->med);
	tab->force_direction = i <= j + 1? i + 1 : -(j + 1);
	// printf("force_direc = %d\nmed = %d\ni = %d --- j = %d\n", tab->force_direction, tab->med, i, j + 1);

}

void ope_first_step(t_tab *tab)  // a renomme en second step
{
	if (tab->a[0] < tab->med)
		// if (tab->a[0] < tab->a[1])
			ope_pb(tab);
		// else
		// {
		// 	ope_sa(tab);
		// }
		
	else
	{
		if (tab->a[0] > tab->a[1])
		{
			if (tab->a[0] > tab->a[tab->index_a]  && !tab->force_direction)
				ope_ra(tab);
			else
				ope_sa(tab);
		}
		else
		{
			if (!tab->force_direction)
				find_next_inf_med(tab);
			else if (tab->force_direction > 0)
			{
				ope_ra(tab);
				tab->force_direction--;
			}
			else
			{
				ope_rra(tab);
				tab->force_direction++;
			}
		}
	}
	try_use_double_ope(tab);
	print_ope(&(tab->ope));
}
