/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 07:12:12 by lachille          #+#    #+#             */
/*   Updated: 2020/03/12 08:04:53 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

/*
** status : 0 -> not sort
**			1 -> sort
**			2 -> empty
*/

void		update_status(t_tab *tab)
{
	int i;

	i = -1;
	tab->a_status = SORT;
	tab->b_status = SORT;
	if (tab->index_a < 0)
		tab->a_status = EMPTY;
	if (tab->index_b < 0)
		tab->b_status = EMPTY;
	while (++i < tab->index_a)
	{
		if (tab->a[i] > tab->a[i + 1])
			tab->a_status = NOT_SORT;
	}
	i = -1;
	while (++i < tab->index_b)
	{
		if (tab->b[i] < tab->b[i + 1])
			tab->b_status = NOT_SORT;
	}
}

static void	init_tab_b(t_tab *tab)
{
	tab->index_b = -1;
	if (!(tab->b = malloc(sizeof(int) * tab->index_a + 1)))
		exit(1);
}

int			check_still_inf_med(t_tab *tab)
{
	int i;

	i = -1;
	while (++i <= tab->index_a)
		if (tab->a[i] < tab->med)
			return (1);
	return (0);
}

void	define_new_med(t_tab *tab)
{
	int i;

	i = -1;
	while (++i != tab->med);
	tab->med = (tab->index_ref + i + 1) / 2;
}

void	fill_a_with_position(t_tab *tab)
{
	int i;
	int j;

	i = -1;
	while (++i <= tab->index_a)
	{
		j = -1;
		while (tab->ref[++j] != tab->a[i]);
		tab->a[i] = j;
	}
}

int	check_if_for_b_sup_med25(t_tab *tab)
{
	int i;

	i = -1;
	while(++i <= tab->index_b)
		if (tab->b[i] >= (tab->index_ref + 1) / 4 )
			return (1);
	return (0);
}


int	check_sort_position(t_tab *tab)
{
	int i;

	i = -1;
	while (++i < tab->index_a)
		if (tab->a[i] - tab->a[i + 1] != -1)
		return (0);
	return (1);
}

void	find_next_b(t_tab *tab)
{
	int i;
	int j;
	int k;
	int max;

	i = -1;
	j = -1;
	k = -1;
	max = -1;
	//find max b
	while (++k <= tab->index_b)
		if (tab->b[k] > max)
			max = tab->b[k];
	while (++i <= tab->index_b && tab->b[i] != max);
	while (++j <= tab->index_b && tab->b[tab->index_b - j] != max);
	j++;
	tab->force_direction = i <= j ? i : -j;
}


int	find_a_reference(t_tab *tab)
{
	int i;

	i = -1;
	while (++i < tab->index_a)
		if (tab->a[tab->index_a - i] - tab->a[tab->index_a - (i + 1)] != 1)
			return (tab->a[tab->index_a - i]);
	return (tab->a[0]);
}
/*
**	faire 1ere partie 50% avec utilisation rr pour mettre b0 < mediane 1/4 a l'arriere
**	puis depush sur b avec la mediane dynamique
**	en fin depush sur b avec les rotate en choisissant An-1 ou An-2 (an-3 et an-4)
**	sb si b1 == an-1 ou an-2
*/
void		solver(t_tab *tab)
{
	int reference;
	init_tab_b(tab);
	fill_a_with_position(tab);
	update_status(tab);
	reset_ope(&(tab->ope));
	tab->force_direction = 0;
	while(check_still_inf_med(tab))
	{
		if (tab->a[0] < tab->med)
			if (tab->b[0] >= (tab->index_ref + 1) / 4 || !check_if_for_b_sup_med25(tab))
				ope_pb(tab);
		if (tab->a[0] >= tab->med)
				ope_ra(tab);
		if (tab->b[0] < (tab->index_ref + 1) / 4 && tab->index_b > 0)
			ope_rb(tab);
		print_ope(&(tab->ope));
	}
	define_new_med(tab);
	while (!check_sort_position(tab))
	{
		ope_first_step(tab);
		update_status(tab);
		if (!check_still_inf_med(tab))
			define_new_med(tab);
	}
	tab->force_direction = 0;
	while (tab->a_status != SORT || tab->b_status != EMPTY) 
	{
		if (tab->a[0] - tab->a[1] == -1 && tab->a[tab->index_a] < tab->a[0] && tab->a[0] - reference == -2)
			ope_rra(tab);
		else if (tab->a[0] - reference == -3 && tab->a[3] != reference)
			ope_ra(tab);
		else if (tab->a[tab->index_a] > tab->a[tab->index_a -1])
			reference = find_a_reference(tab);
		find_next_b(tab);
		if (tab->a[0] - tab->a[1] == 1)
		{
			ope_sa(tab);
			if (tab->b[0] < tab->b[1])
				ope_sb(tab);
		}
		
		else if (tab->b[0] - reference <= 1 && tab->b[0] - reference >= -3)
			ope_pa(tab);
		else if (tab->force_direction > 0)
		{
			ope_rb(tab);
			tab->force_direction--;
		}
		else if (tab->force_direction < 0)
		{
			ope_rrb(tab);
			tab->force_direction++;
		}
		print_ope(&(tab->ope));
		update_status(tab);
		// if (tab->a_status == SORT && tab->b_status == EMPTY)
		// 	break ;
	}
	// print_tab(tab);
}
