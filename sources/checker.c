/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/20 02:37:30 by lachille          #+#    #+#             */
/*   Updated: 2020/03/12 06:49:27 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static int	ope_finder2(int *res, int *i, char *str, int old_i)
{
	while (str[*i] != '\n' && str[*i])
	{
		if (str[*i] == 'p')
			*res += 4;
		else if (str[*i] == 's')
			*res += 8;
		else if (str[*i] == 'r')
			*res += 32;
		else if (*i > old_i && (str[*i] == 'a' || str[*i] == 'b'))
		{
			if (str[*i] == 'a')
				*res += 1;
			else if (str[*i] == 'b')
				*res += 2;
			// *i += 1;
			if (str[*i + 1] != '\n' && str[*i])
				return (0);
		}
		else
			return (0);
		*i += 1;
	}
	// *i += 1;
	return (1);
}

int			check_res_validity(int res)
{
	if (res != PA && res != PB && res != SA && res != SB && res != SS
	 && res != RA && res != RB && res != RR && res != RRA
	 && res != RRB && res != RRR && res)
		return (0);
	return (1);
}

int			ope_finder(char *str, int *i)
{
	int res;
	int old_i;

	old_i = *i;
	res = 0;
	if (!ope_finder2(&res, i, str, old_i) || !check_res_validity(res))
	{
		ft_putstr_fd("Wrong command : ", 2);
		write(2, str + old_i, (*i + 2) - old_i);
		write(2, "\n", 1);
		return (0);
	}
	if (!res)
		return (-1);
	if (str[*i] != '\n')
		return (0);
	*i += 1;
	return (res);
}

void		check_ope(t_tab *tab, t_check chk)
{
	if (chk.ope == PA)
		ope_pa(tab);
	if (chk.ope == PB)
		ope_pb(tab);
	if (chk.ope == SA || chk.ope == SS)
		ope_sa(tab);
	if (chk.ope == SB || chk.ope == SS)
		ope_sb(tab);
	if (chk.ope == RA || chk.ope == RR)
		ope_ra(tab);
	if (chk.ope == RB || chk.ope == RR)
		ope_rb(tab);
	if (chk.ope == RRA || chk.ope == RRR)
		ope_rra(tab);
	if (chk.ope == RRB || chk.ope == RRR)
		ope_rrb(tab);
	chk.opt & 1 ? opt_print(chk, *tab) : 0;
}

void		init_checker(t_check *chk, t_tab *tab)
{
	tab->index_b = -1;
	chk->i = 0;
	chk->ope = 0;
	chk->size_buf = 32;
}
