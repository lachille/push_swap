/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 01:39:15 by lachille          #+#    #+#             */
/*   Updated: 2020/03/03 05:35:31 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ope_ra(t_tab *tab)
{
	int tmp;
	int i;

	if (tab->index_a > 0)
	{
		i = -1;
		tmp = tab->a[0];
		while (++i < tab->index_a)
			tab->a[i] = tab->a[i + 1];
		tab->a[tab->index_a] = tmp;
		tab->ope.ra = 1;
	}
}

void	ope_rra(t_tab *tab)
{
	int tmp;
	int i;

	if (tab->index_a > 0)
	{
		i = tab->index_a + 1;
		tmp = tab->a[tab->index_a];
		while (--i > 0)
			tab->a[i] = tab->a[i - 1];
		tab->a[0] = tmp;
		tab->ope.rra = 1;
	}
}

void	ope_rb(t_tab *tab)
{
	int tmp;
	int i;

	i = -1;
	if (tab->index_b > 0)
	{
		tmp = tab->b[0];
		while (++i < tab->index_b)
			tab->b[i] = tab->b[i + 1];
		tab->b[tab->index_b] = tmp;
		tab->ope.rb = 1;
	}
}

void	ope_rrb(t_tab *tab)
{
	int tmp;
	int i;

	if (tab->index_b > 0)
	{
		i = tab->index_b + 1;
		tmp = tab->b[tab->index_b];
		while (--i > 0)
			tab->b[i] = tab->b[i - 1];
		tab->b[0] = tmp;
		tab->ope.rrb = 1;
	}
}
