/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/24 04:17:43 by lachille          #+#    #+#             */
/*   Updated: 2020/03/12 05:30:28 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void			opt_print(t_check chk, t_tab tab)
{
	if (chk.ope == PA)
		ft_putstr("PA\n");
	if (chk.ope == PB)
		ft_putstr("PB\n");
	if (chk.ope == SA)
		ft_putstr("SA\n");
	if (chk.ope == SB)
		ft_putstr("SB\n");
	if (chk.ope == SS)
		ft_putstr("SS\n");
	if (chk.ope == RA)
		ft_putstr("RA\n");
	if (chk.ope == RB)
		ft_putstr("RB\n");
	if (chk.ope == RR)
		ft_putstr("RR\n");
	if (chk.ope == RRA)
		ft_putstr("RRA\n");
	if (chk.ope == RRB)
		ft_putstr("RRB\n");
	if (chk.ope == RRR)
		ft_putstr("RRR\n");
	print_tab(&tab);
	ft_putstr("\e[0m\n");
}

void			print_output(t_check chk, t_tab *tab)
{	
	ft_putstr("Push_swap Output:\n");
	ft_putstr(chk.dst);
	ft_putstr("END\n\n");
	print_tab(tab);
	ft_putstr("\e[0m\n");
}

void			print_res(t_tab tab, t_check chk)
{
	update_status(&tab);
	if (tab.a_status == SORT && tab.b_status == EMPTY)
		ft_putstr("OK\n");
	else
		ft_putstr("KO\n");
	chk.opt & 1 ? print_tab(&tab) : 0;
}

static void		print_ope2(t_ope *ope)
{
	if (ope->rra && ope->rrb)
		ft_putstr("rrr\n");
	else if (ope->rra)
		ft_putstr("rra\n");
	else if (ope->rrb)
		ft_putstr("rrb\n");
	if (ope->pa)
	{
		ope->pa = 0;
		ft_putstr("pa\n");
	}
}

void			print_ope(t_ope *ope)
{
	if (ope->pb)
	{
		ope->pb = 0;
		ft_putstr("pb\n");
	}
	if (ope->sa && ope->sb)
		ft_putstr("ss\n");
	else if (ope->sa)
		ft_putstr("sa\n");
	else if (ope->sb)
		ft_putstr("sb\n");
	if (ope->ra && ope->rb)
		ft_putstr("rr\n");
	else if (ope->ra)
		ft_putstr("ra\n");
	else if (ope->rb)
		ft_putstr("rb\n");
	print_ope2(ope);
	reset_ope(ope);
}
