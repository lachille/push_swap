/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/12 22:34:03 by lachille          #+#    #+#             */
/*   Updated: 2020/03/12 05:43:51 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/libft.h"
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>

typedef	struct	s_ope
{
	int sa;
	int sb;

	int ra;
	int rb;

	int rra;
	int rrb;

	int pa;
	int pb;
}				t_ope;

typedef	struct	s_tab
{
	int		*a;
	int		index_a;
	int		a_status;
	int		*b;
	int		index_b;
	int		b_status;
	int		*ref;
	int		index_ref;
	int		med;
	int		force_direction;
	t_ope	ope;
}				t_tab;

typedef	struct	s_sort{
	int i;
	int j;
	int k;
}				t_sort;

typedef enum	e_status
{
	NOT_SORT,
	SORT,
	EMPTY,
}				t_status;

/*
** Value of each letter :
**  A = 1
**  B = 2
**  P = 4
**  S = 8
**  R* = 32
*/
typedef enum	e_operation
{
	PA = 5,
	PB = 6,

	SA = 9,
	SB = 10,
	SS = 16,

	RA = 33,
	RB = 34,
	RR = 64,

	RRA = 65,
	RRB = 66,
	RRR = 96,
}				t_operation;

typedef	struct	s_check
{
	char			*buf;
	char			*dst;
	int				ope;
	unsigned int	size_buf;
	int				i;
	int				opt;
}				t_check;

/*
** In ope.c
*/
void			reset_ope(t_ope *ope);
void			try_use_double_ope(t_tab *tab);
void			ope_first_step(t_tab *tab);
/*
** In push.c
*/
void			ope_pa(t_tab *tab);
void			ope_pb(t_tab *tab);
/*
** In swap.c
*/
void			ope_sa(t_tab *tab);
void			ope_sb(t_tab *tab);
/*
** In rotate.c
*/
void			ope_ra(t_tab *tab);
void			ope_rra(t_tab *tab);
void			ope_rb(t_tab *tab);
void			ope_rrb(t_tab *tab);
/*
** In parsing.c
*/
int				fill_tab(char **src, t_tab *tab, int mode);
int				check(char **av, int mode);

/*
** In solver.c
*/
void			solver(t_tab *tab);
void			update_status(t_tab *tab);
/*
** In sort.c
*/
void			merge_sort(int *arr, int start, int end);
/*
** In utils.c
*/
void			print_tab(t_tab *tab);
int				doublons_checker(t_tab *tab);
int				is_nrvt(char c);
void			opt_handler(t_check *chk, char **av, int *fd);
/*
** In print.c
*/
void			opt_print(t_check chk, t_tab tab);
void			print_output(t_check chk, t_tab *tab);
void			print_res(t_tab tab, t_check chk);
void			print_ope(t_ope *ope);
/*
** In checker.c
*/
void			init_checker(t_check *chk, t_tab *tab);
void			check_ope(t_tab *tab, t_check chk);
int				ope_finder(char *str, int *i);

#endif
