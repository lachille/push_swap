/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 07:24:11 by lachille          #+#    #+#             */
/*   Updated: 2020/02/19 10:02:31 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long	ft_atol(char const *str)
{
	int		index;
	int		signe;
	long	res;

	index = 0;
	signe = 1;
	res = 0;
	while (ft_isesc(str[index]))
		index++;
	if (str[index] == '-' || str[index] == '+')
	{
		if (str[index] == '-')
			signe = -1;
		index++;
	}
	while (ft_isdigit(str[index]))
	{
		res = res * 10 + str[index] - 48;
		index++;
	}
	return (res * signe);
}
