/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc_line.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 07:37:45 by lachille          #+#    #+#             */
/*   Updated: 2020/02/19 10:02:00 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**realloc_line(char **old, unsigned int *buf)
{
	char			**new;
	unsigned int	buf2;
	int				i;

	buf2 = *buf;
	*buf *= 2;
	i = -1;
	if (*buf < buf2 || !(new = malloc(sizeof(char *) * *buf)))
		return (0);
	while (++i < (int)*buf)
	{
		if (i < (int)buf2)
		{
			if (!(new[i] = ft_strdup(old[i])))
				return (0);
			free(old[i]);
		}
	}
	free(old);
	return (new);
}
